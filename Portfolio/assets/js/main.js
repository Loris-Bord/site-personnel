/**
* Template Name: Laura - v4.10.0
* Template URL: https://bootstrapmade.com/laura-free-creative-bootstrap-theme/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    if (!header.classList.contains('header-scrolled')) {
      offset -= 20
    }

    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos - offset,
      behavior: 'smooth'
    })
  }

  /**
   * Toggle .header-scrolled class to #header when page is scrolled
   */
  let selectHeader = select('#header')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
      } else {
        selectHeader.classList.remove('header-scrolled')
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Mobile nav dropdowns activate
   */
  on('click', '.navbar .dropdown > a', function(e) {
    if (select('#navbar').classList.contains('navbar-mobile')) {
      e.preventDefault()
      this.nextElementSibling.classList.toggle('dropdown-active')
    }
  }, true)

  /**
   * Scrool with ofset on links with a class name .scrollto
   */
  on('click', '.scrollto', function(e) {
    if (select(this.hash)) {
      e.preventDefault()

      let navbar = select('#navbar')
      if (navbar.classList.contains('navbar-mobile')) {
        navbar.classList.remove('navbar-mobile')
        let navbarToggle = select('.mobile-nav-toggle')
        navbarToggle.classList.toggle('bi-list')
        navbarToggle.classList.toggle('bi-x')
      }
      scrollto(this.hash)
    }
  }, true)

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Skills animation
   */
  let skilsContent = select('.skills-content');
  if (skilsContent) {
    new Waypoint({
      element: skilsContent,
      offset: '80%',
      handler: function(direction) {
        let progress = select('.progress .progress-bar', true);
        progress.forEach((el) => {
          el.style.width = el.getAttribute('aria-valuenow') + '%'
        });
      }
    })
  }

  /**
   * Testimonials slider
   */
  new Swiper('.testimonials-slider', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Porfolio isotope and filter
   */
  window.addEventListener('load', () => {
    let portfolioContainer = select('.portfolio-container');
    if (portfolioContainer) {
      let portfolioIsotope = new Isotope(portfolioContainer, {
        itemSelector: '.portfolio-item'
      });

      let portfolioFilters = select('#portfolio-flters li', true);

      on('click', '#portfolio-flters li', function(e) {
        e.preventDefault();
        portfolioFilters.forEach(function(el) {
          el.classList.remove('filter-active');
        });
        this.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: this.getAttribute('data-filter')
        });

      }, true);
    }

  });

  /**
   * Initiate portfolio lightbox 
   */
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });

  /**
   * Portfolio details slider
   */
  new Swiper('.portfolio-details-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Initiate Pure Counter 
   */
  new PureCounter();

})()

/**
 * Barre de progression dans la page
 */
window.onload = () => {

  const progressSize = () => {
     // Calcul de la hauteur "utile" du document
     let hauteur = document.documentElement.scrollHeight - window.innerHeight;
     // Récupération de la position verticale
     let position = window.scrollY;
     // Récupération de la largeur de la fenêtre
     let largeur = document.documentElement.clientWidth;

     let barre = position / hauteur * largeur;

     document.getElementById("progress").style.width = barre+"px";
  }

  window.addEventListener('scroll', progressSize);

}


/**
 * Calcul la position de l'élément par rapport au haut de la page
 * @param {HTMLElement} element
 * @return {number}
 */
function offsetTop(element, acc = 0) {
  if (element.offsetParent) {
    return offsetTop(element.offsetParent, acc + element.offsetTop);
  }
  return acc + element.offsetTop;
}

const modal = document.getElementById('modal');
const premierProjet = document.getElementById('premierProjet');

const button1 = document.getElementById('button1');

premierProjet.addEventListener("click", openModal);
button1.addEventListener("click", closeModal);

function openModal() {
  document.body.classList.add('modal-open');
  modal.style.display = 'block';
  setTimeout(() => modal.style.opacity = 1, 0);
}

function closeModal() {
  document.body.classList.remove('modal-open');
  modal.style.display = 'none';
  setTimeout(() => modal.style.opacity = 0, 0);
}


/*Deuxième fenetre modal */

const modal1 = document.getElementById('modal1');
const deuxiemeProjet = document.getElementById('deuxiemeProjet');

const button11 = document.getElementById('button11');

deuxiemeProjet.addEventListener("click", openModal1);
button11.addEventListener("click", closeModal1);
//modal1.addEventListener("click", closeModal1);

function openModal1() {
  document.body.classList.add('modal-open');
  modal1.style.display = 'block';
  setTimeout(() => modal1.style.opacity = 1, 0);
}

function closeModal1() {
  document.body.classList.remove('modal-open');
  modal1.style.display = 'none';
  setTimeout(() => modal1.style.opacity = 0, 0);
}

/* Troisième fenetre modal */

const modal2 = document.getElementById('modal2');
const troisiemeProjet = document.getElementById('troisièmeProjet');

const button12 = document.getElementById('button12');

troisiemeProjet.addEventListener("click", openModal2);
button12.addEventListener("click", closeModal2);
//modal1.addEventListener("click", closeModal1);

function openModal2() {
  document.body.classList.add('modal-open');
  modal2.style.display = 'block';
  setTimeout(() => modal2.style.opacity = 1, 0);
}

function closeModal2() {
  document.body.classList.remove('modal-open');
  modal2.style.display = 'none';
  setTimeout(() => modal2.style.opacity = 0, 0);
}

/*                */


const modal3 = document.getElementById('modal3');
const quatriemeProjet = document.getElementById('quatrièmeProjet');

const button13 = document.getElementById('button13');

quatriemeProjet.addEventListener("click", openModal3);
button13.addEventListener("click", closeModal3);
//modal1.addEventListener("click", closeModal1);

function openModal3() {
  document.body.classList.add('modal-open');
  modal3.style.display = 'block';
  setTimeout(() => modal3.style.opacity = 1, 0);
}

function closeModal3() {
  document.body.classList.remove('modal-open');
  modal3.style.display = 'none';
  setTimeout(() => modal3.style.opacity = 0, 0);
}

/*                */
const modal4 = document.getElementById('modal4');
const cinquiemeProjet = document.getElementById('cinquièmeProjet');

const button14 = document.getElementById('button14');

cinquiemeProjet.addEventListener("click", openModal4);
button14.addEventListener("click", closeModal4);
//modal1.addEventListener("click", closeModal1);

function openModal4() {
  document.body.classList.add('modal-open');
  modal4.style.display = 'block';
  setTimeout(() => modal4.style.opacity = 1, 0);
}

function closeModal4() {
  document.body.classList.remove('modal-open');
  modal4.style.display = 'none';
  setTimeout(() => modal4.style.opacity = 0, 0);
}

/*      */

const modal5 = document.getElementById('modal5');
const sixiemeProjet = document.getElementById('sixiemeProjet');

const button15 = document.getElementById('button15');

sixiemeProjet.addEventListener("click", openModal5);
button15.addEventListener("click", closeModal5);
//modal1.addEventListener("click", closeModal1);

function openModal5() {
  document.body.classList.add('modal-open');
  modal5.style.display = 'block';
  setTimeout(() => modal5.style.opacity = 1, 0);
}

function closeModal5() {
  document.body.classList.remove('modal-open');
  modal5.style.display = 'none';
  setTimeout(() => modal5.style.opacity = 0, 0);
}